<?php
/**
 * @file
 * acquia.settings.php
 *
 * This settings file is intended to contain settings specific to Acquia servers,
 * by overriding options set in settings.php.
 *
 * Include this file from your regular settings.php by including this at the
 * bottom:
 *
 * require 'acquia.settings.php';
 */

// Name of Acquia DB.
$acquia_db_name = $config['instance_settings.settings']['site_dir'];

$acquia_db_name = $acquia_db_name == 'default' ? 'aroq' : $acquia_db_name;

// Required to connect to Acquia DB.
// Not included on other servers.
if (file_exists('/var/www/site-php')) {
  $db_name = str_replace('.', '_', $acquia_db_name);
  $db_name = str_replace('-', '_', $db_name);
  require_once "/var/www/site-php/aroq/$db_name-settings.inc";
}

// Provide environment for Acquia servers.
if (getenv('AH_SITE_ENVIRONMENT')) {
  $config['instance_settings.settings']['environment'] = getenv('AH_SITE_ENVIRONMENT');
//  $files_private_conf_path = conf_path();
//  $conf['file_private_path'] = '/mnt/files/' . getenv('AH_SITE_GROUP') . '.' . getenv('AH_SITE_ENVIRONMENT') . '/' . $files_private_conf_path . '/files-private';
}
elseif (file_exists('/var/www/site-scripts/site-info.php')) {
  require_once '/var/www/site-scripts/site-info.php';
  list($bah_site_name, $bah_site_group, $bah_site_stage, $secret) = ah_site_info();
  $config['instance_settings.settings']['environment'] = $bah_site_stage;
}

