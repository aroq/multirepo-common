<?php
/**
 * @file
 * This settings file should be included from include.settings.inc file.
 * Put here settings related to ALL instances.
 */

/**
 * Define services definition file suggestions.
 */

// Global services
$settings['container_yamls'][] = __DIR__ . "/services.yml";

// Global local services (ignored by git)
$settings['container_yamls'][] = __DIR__ . "/ignored.services.yml";


// Environment-specific services
$settings['container_yamls'][] = __DIR__ . "/{$config['instance_settings.settings']['environment']}.services.yml";

// Environment-specific local services (ignored by git)
$settings['container_yamls'][] = __DIR__ . "/ignored.{$config['instance_settings.settings']['environment']}.services.yml";
