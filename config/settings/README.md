# README

## About

This directory is the central place for editing both global and 
environment-specific instances configuration.

It contains a set of Drupal settings PHP files and service definition
YAML files. They are included depending on the execution environment 
during Drupal bootstrap to provide an easy to maintain instances 
configuration.

## settings.php overrides  

The setting files are included in the following order:

  1. `all.settings.inc` - included always 
  2. `local.settings.inc` - included for `environment = local`
  3. `acquia.settings.inc` - included always
  4. `final.ignored.settings.inc` - included always, *if exists*.
  5. `final.ignored.local.settings.inc` - included for `environment = local`, *if exists*.
 
Last two are optional and are ignored in `.gitignore` by default. Use it 
for local overrides which you generally place in `settings.php`.

## services.yml overrides

The following services definition files are searched for inclusion:

  * `services.yml` - included always. 
  * `local.services.yml` - included for `environment = local`

Their fellows `ignored.services.yml` and `ignored.local.services.yml` 
do the same but are ignored by git and intended for local overrides.

## environments

The following environments are defined:
 
 * **local** (default)
 * dev
 * test
 
To switch the environment set the shell environment variable `AH_SITE_ENVIRONMENT`, e.g.:
 
```
$ AH_SITE_ENVIRONMENT=test drush sql-cli
```
