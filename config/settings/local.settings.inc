<?php

/**
 * @file
 * local.settings.php
 *
 * This settings file is intended to contain settings specific to a development
 * environment, by overriding options set in settings.php.
 */

/**
 * Disable CSS and JS aggregation.
 */
$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;
