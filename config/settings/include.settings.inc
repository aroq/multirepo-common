<?php
/**
 * @file
 * This settings file should be included from settings.php file.
 */

// Taken from Acquia-side settings file for aroq docroot.
$settings['hash_salt'] = '3acc2588a71c5a72214b03bb45d3783a91eba7392c76ede5881abb6ad1841b62';

$config['instance_settings.settings']['env_get_method'] = 'acquia';
$config['instance_settings.settings']['pattern_types'] =  array(
  'default' => array(
    'acquia',
  ),
  'final' => array(
    'final.ignored',
    'final.ignored.!environment',
    'acquia_purge',
  ),
);

$config['instance_settings.settings']['included_files'][] = DRUBONE_ROOT . '/code/common/config/settings/include.settings.inc';
$config['instance_settings.settings']['included_files'][] = DRUBONE_DIR_FULL . '/includes/drupal8.include.settings.inc';

if (file_exists(DRUBONE_DIR_FULL . '/includes/drupal8.include.settings.inc')) {
  include_once DRUBONE_DIR_FULL . '/includes/drupal8.include.settings.inc';
}

$settings['install_profile'] = 'lightning';

// Override any Acquia settings here.

if ($config['instance_settings.settings']['site_dir'] == 'default.migrate') {
  $config_directories['sync'] = '../code/default/config/sync';
}
else {
  $config_directories['sync'] = '../code/' . $config['instance_settings.settings']['site_dir'] . '/config/sync';
}


// TODO: Needs to be re-checked.
// Copied from Acquia settings, but need to check how it should be working.
// This setting gives error on status page.
if (!drupal_installation_attempted()) {
  $config_directories['vcs'] = $app_root . '/../code/' . $config['instance_settings.settings']['site_dir'] . '/config/sync';
}




