<?php
/**
 * @file
 * stage.settings.php
 *
 * This settings file is intended to contain settings specific to a staging
 * environment, by overriding options set in settings.php.
 */

$config['system.performance']['css']['preprocess'] = TRUE;
$config['system.performance']['js']['preprocess'] = TRUE;
