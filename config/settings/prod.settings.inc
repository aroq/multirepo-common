<?php
/**
 * @file
 * prod.settings.php (Drupal 7.x)
 *
 * This settings file is intended to contain settings specific to a production
 * environment, by overriding options set in settings.php.
 */

/**
 * Enable CSS and JS aggregation.
 */
$config['system.performance']['css']['preprocess'] = TRUE;
$config['system.performance']['js']['preprocess'] = TRUE;
